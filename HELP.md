docker build --tag gateway .

docker network create -d bridge gateway-net

docker run -dit \
--name gateway-server \
--hostname gatewayhost \
--network gateway-net \
-p 8081:8081 \
gateway \
--server.port=8081


Поддерживаемые запросы

URL-адрес	Тело

http://gatewayhost:8081/api/v1/create_game	{ "admin_id": 123 }
http://gatewayhost:8081/api/v1/connect_to_game	{ "code": "76A8F1", "player_id": 234 }
http://gatewayhost:8081/api/v1/add_card	{ "player_id": 345, "card_id": "AgA4Ag1AAxk5A" }
http://gatewayhost:8081/api/v1/get_player_cards	{ "player_id": 345 }
http://gatewayhost:8081/api/v1/delete_player_card	{ "player_id": 345, "card_id": "AgA4Ag1AAxk5A" }
http://gatewayhost:8081/api/v1/get_lobby_info	{ "code": "76A8F1" }
http://gatewayhost:8081/api/v1/leave_game	{ "code": "76A8F1", "player_id": 345, "for_all": false }
http://gatewayhost:8081/api/v1/start_game	{ "code": "76A8F1" }
http://gatewayhost:8081/api/v1/get_game_info	{ "code": "76A8F1" }
http://gatewayhost:8081/api/v1/get_round_cards	{ "code": "76A8F1" }
http://gatewayhost:8081/api/v1/get_hand	{ "code": "76A8F1", "player_id": 234 }
http://gatewayhost:8081/api/v1/send_riddle	{ "code": "76A8F1", "player_id": 234, "riddle": "smth hot!", "card_id": "AgA4Ag" }
http://gatewayhost:8081/api/v1/send_association	{ "code": "76A8F1", "player_id": 234, "card_id": "AgA4Ag" }
http://gatewayhost:8081/api/v1/send_guess	{ "code": "76A8F1", "player_id": 234, "card_id": "AgA4Ag" }
http://gatewayhost:8081/api/v1/authorize   { "user_id": 123 }