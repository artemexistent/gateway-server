package ua.knu.getaway.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
@Slf4j
public class ReactiveRedisOperationsServiceImpl implements ReactiveRedisOperationsService {
  private final ReactiveRedisOperations<String, String> players;

  @Override
  public Mono<String> getPlayerInfoFromRedis(String authToken) {
    log.info("Retrieving player info from Redis");
    return players.opsForValue().get(authToken)
        .doOnNext(t -> log.info("found user {} in redis", t));
  }
}
