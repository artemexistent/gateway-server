package ua.knu.getaway.service;

import reactor.core.publisher.Mono;

public interface ReactiveRedisOperationsService {
  Mono<String> getPlayerInfoFromRedis(String authToken);
}
