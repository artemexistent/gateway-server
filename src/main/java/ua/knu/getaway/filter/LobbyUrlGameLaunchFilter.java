package ua.knu.getaway.filter;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebInputException;
import ua.knu.getaway.service.ReactiveRedisOperationsService;

import java.util.Optional;

import static java.util.Objects.isNull;

@Component
public class LobbyUrlGameLaunchFilter extends AbstractGatewayFilterFactory<LobbyUrlGameLaunchFilter.Config> {

  private static final String AUTHORIZATION_HEADER = "Authorization";
  private static final String PLAYER_HEADER = "user-id";
  private final ReactiveRedisOperationsService reactiveRedisOperationsService;

  public LobbyUrlGameLaunchFilter(ReactiveRedisOperationsService reactiveRedisOperationsService) {
    super(LobbyUrlGameLaunchFilter.Config.class);
    this.reactiveRedisOperationsService = reactiveRedisOperationsService;
  }

  @Override
  public GatewayFilter apply(LobbyUrlGameLaunchFilter.Config config) {
    return (exchange, chain) -> {
      ServerHttpRequest httpRequest = exchange.getRequest();

      final var token = Optional.ofNullable(httpRequest.getHeaders().get(AUTHORIZATION_HEADER))
          .flatMap(headers -> headers.stream().findFirst())
          .orElse(null);

      if (isNull(token)) {
        throw new ServerWebInputException("Required String parameter 'token' is not present");
      }

      return reactiveRedisOperationsService.getPlayerInfoFromRedis(token)
          .map(playerInfo -> httpRequest.mutate().header(PLAYER_HEADER, playerInfo).build())
          .flatMap(serverHttpRequest -> chain.filter(exchange.mutate().request(serverHttpRequest).build()));
    };
  }

  public static class Config {
    // WTF
  }
}